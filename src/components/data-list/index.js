import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { FaSearch } from 'react-icons/fa'

import { selecionarItemLista } from '../../store/action/selecionar-itens-lista'

class DataList extends Component {
	createDataList() {
		return this.props.valores.map(item => (
			<li
				className='list-group-item list-group-item-action'
				onClick={() => this.props.selecionarItemLista(item)}
				key={item.id}>
				{item.codTurma} - {item.descricaoTurma}
			</li>
		))
	}

	render() {
		return (
			<div>
				<div className='input-group'>
					<div className='input-group-prepend'>
						<span className='input-group-text' id='basic-addon1'>
							<FaSearch />
						</span>
					</div>
					<input type='text' className='form-control' />
				</div>
				<div>
					<div className='list-group list-group-flush'>
						<ul>{this.createDataList()}</ul>
					</div>
				</div>
			</div>
		)
	}
}

const mapStateToProps = state => ({
	valores: state.dataListReducer.valores,
})

const mapDispatchToProps = dispatch => bindActionCreators({ selecionarItemLista: selecionarItemLista }, dispatch)

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(DataList)
