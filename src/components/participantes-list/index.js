import React, { Component } from 'react'
import { Table } from 'semantic-ui-react'
import firebase from '../../service/api'
import Skeleton from 'react-loading-skeleton'
const SkeletonLine = () => <Skeleton />

export default class ParticipantesList extends Component {
	constructor(props) {
		super(props)
		this.state = { lista: [], loading: false }
	}

	componentDidMount() {
		const db = firebase.firestore()
		db.collection('registro-palestras').onSnapshot(spnapshot => {
			let state = this.state
			state.lista = []
			spnapshot.forEach(doc => {
				state.lista.push({
					key: doc.id,
					nome: doc.data().nome,
					email: doc.data().email,
					data: doc
						.data()
						.data.toDate()
						.toLocaleString(),
				})
			})
			state.loading = true
			this.setState(state)
		})
	}

	createList() {
		let profileContent = null

		if (this.state.loading === false) {
			profileContent = (
				<Table.Row>
					<Table.Cell>
						<SkeletonLine duration={50} />
					</Table.Cell>
					<Table.Cell>
						<SkeletonLine duration={50} />
					</Table.Cell>
					<Table.Cell>
						<SkeletonLine duration={50} />
					</Table.Cell>
				</Table.Row>
			)
		} else {
			profileContent = this.state.lista.map(item => (
				<Table.Row key={item.key}>
					<Table.Cell>{item.nome}</Table.Cell>
					<Table.Cell>{item.email}</Table.Cell>
					<Table.Cell>{item.data} </Table.Cell>
				</Table.Row>
			))
		}
		return profileContent
	}

	render() {
		return (
			<div>
				<Table columns={5}>
					<Table.Header>
						<Table.Row>
							<Table.HeaderCell>Nome</Table.HeaderCell>
							<Table.HeaderCell>Email</Table.HeaderCell>
							<Table.HeaderCell>DataRegistro</Table.HeaderCell>
						</Table.Row>
					</Table.Header>
					<Table.Body>{this.createList()}</Table.Body>
					<Table.Footer>
						<Table.Row>
							<Table.HeaderCell>
								Quantidade: {this.state.lista.length}
								{/*<button type='button' className='btn btn-outline-success ml-4'>
											Sortear
											</button>*/}
							</Table.HeaderCell>
							<Table.HeaderCell />
							<Table.HeaderCell colSpan='3'></Table.HeaderCell>
						</Table.Row>
					</Table.Footer>
				</Table>
			</div>
		)
	}
}
