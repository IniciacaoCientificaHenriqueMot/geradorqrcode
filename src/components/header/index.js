import React from 'react'
import { Link } from 'react-router-dom'
import { FaHome, FaUsers, FaUniversity } from 'react-icons/fa'
import './styles.css'

const Header = props => (
	<header className='navbar  p-2 w-100 justify-content-between' style={{ backgroundColor: props.bg }}>
		<div>
			<a className='navbar-brand main-icon ml-3'>
				<Link to='/'>
					<FaHome />
				</Link>
			</a>
			<div className='navbar-text main-text align-top'>{props.name}</div>
		</div>
		<div>
			<a className='navbar-brand main-icon ml-3'>
				<Link to='/palestra'>
					<FaUsers />
				</Link>
			</a>
			<a className='navbar-brand main-icon ml-3'>
				<Link to='/frequencia'>
					<FaUniversity />
				</Link>
			</a>
		</div>
	</header>
)

export default Header
