import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Modal, ModalBody, } from 'reactstrap';
import QRCode from '../qrcode';
import './styles.css'
import DataList from '../../components/data-list';


class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    }
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  render() {
    return (
      <div>
        <div className="container" id="bodyForm">
          <div className="card justify-content-center mt-5">
            <div className="row no-gutters">
              <div className="col-md col-sm-12 p-3" id="colQRform">
                <form >
                  <div>
                    <label>Código turma</label>
                    <input type="text" className="form-control"
                      value={this.props.valores.codTurma} />
                  </div>
                  <div>
                    <label>Descrição da turma</label>
                    <input type="text" className="form-control"
                      value={this.props.valores.descricaoTurma} />
                  </div>
                  <div>
                    <label>Dias da Semana</label>
                    <input type="text" className="form-control"
                      value={this.props.valores.diasSemanas} />
                  </div>
                  <div>
                    <label>Horário</label>
                    <input type="text" className="form-control"
                      value={this.props.valores.horario} />
                  </div>
                  <div>
                    <label>Professor</label>
                    <input type="text" className="form-control"
                      value={this.props.valores.professor} />
                  </div>
                  <div>
                    <label>Unidade</label>
                    <input type="text" className="form-control"
                      value={this.props.valores.unidade} />
                  </div>
                  <div className="mt-3">
                    <button type="button" className="btn btn-primary" onClick={this.toggle}>
                      Gerar
                      </button>
                  </div>
                </form>
              </div>
              <div className="align-self-center p-3 col-5">
                <div id="data-list" className="justify-content-stretch m-3 ">
                  <DataList />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="QRModal">
          <Modal isOpen={this.state.modal} toggle={this.toggle}
            modalTransition={{ timeout: 200 }} className="QRModalContent">
            <ModalBody>
              <QRCode array={
                this.props.valores
              } />
            </ModalBody>
          </Modal>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  valores: state.itemSelecionado
});

export default connect(mapStateToProps)(index);