import React from 'react'
import { QRCode } from 'react-qr-svg'

const index = props => {
	return (
		<div>
			<QRCode level='Q' style={props.style} value={props.array} />
		</div>
	)
}

export default index
