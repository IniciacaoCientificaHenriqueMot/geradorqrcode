import firebase from 'firebase'
import 'firebase/firestore'

let firebaseConfig = {
	apiKey: 'AIzaSyC7vv1SrlR9yHggPBa-DjeEBKHGjJKukXY',
	authDomain: 'registro-frequencia.firebaseapp.com',
	databaseURL: 'https://registro-frequencia.firebaseio.com',
	projectId: 'registro-frequencia',
	storageBucket: '',
	messagingSenderId: '837329377368',
	appId: '1:837329377368:web:5ddebbe6bf16dab8d6122d',
}
firebase.initializeApp(firebaseConfig)
export default firebase
