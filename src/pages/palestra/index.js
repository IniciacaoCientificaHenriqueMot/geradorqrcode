import React, { Component } from 'react'
import Header from '../../components/header'

import { connect } from 'react-redux'
import { itensPalestras } from '../../store/action/palestra-itens'

import { Card } from 'semantic-ui-react'
import { Modal, ModalBody } from 'reactstrap'
import QRCode from '../../components/qrcode'
import firebase from '../../service/api'
import ParticipantesList from '../../components/participantes-list'

class index extends Component {
	constructor(props) {
		super(props)
		this.state = {
			palestrante: '',
			titulo: '',
			local: '',
			dataHora: '',
			modal: false,
		}
	}

	toggle() {
		let camposValidos = ['palestrante', 'titulo', 'local', 'dataHora']
		let state = this.state
		let empty = false
		for (const [key, value] of Object.entries(state)) {
			if (camposValidos.indexOf(key)) {
				if (value == null) {
					empty = true
				}
			}
		}
		if (!empty) {
			this.setState({
				modal: !this.state.modal,
			})
		}
	}

	handleChange = e => {
		this.setState({ [e.target.id]: e.target.value })
	}

	handleSubmit = e => {
		e.preventDefault()
		// const db = firebase.firestore()
		// db.settings({
		// 	timestampsInSnapshots: true,
		// })
		// db.collection('palestras').add({
		// 	palestrante: this.state.palestrante,
		// 	titulo: this.state.titulo,
		// 	local: this.state.local,
		// 	dataHora: this.state.dataHora,
		// })
		this.props.dispatchItensPalestras(this.state)
	}

	render() {
		const { palestrante, titulo, local, dataHora } = this.state
		return (
			<div>
				<Header name='Palestras' bg='#279566' />
				<div className='container'>
					<Card fluid className='card-body mt-2 p-2'>
						<form onSubmit={this.handleSubmit}>
							<div className='form-row m-2 '>
								<div className='ml-2 col'>
									<div className='title mb-2'>
										<label>Palestrantes:</label>
									</div>
									<div>
										<input
											type='text'
											className='form-control'
											id='palestrante'
											onChange={this.handleChange}
											value={palestrante}
											// required
										/>
									</div>
								</div>
								<div className='ml-2 col'>
									<div className='title mb-2'>
										<label>Título:</label>
									</div>
									<div>
										<input
											type='text'
											className='form-control'
											id='titulo'
											onChange={this.handleChange}
											value={titulo}
											// required
										/>
									</div>
								</div>
							</div>
							<div className='form-row m-2'>
								<div className='ml-2 col'>
									<div className='title mb-2'>
										<label>Local:</label>
									</div>
									<div>
										<input
											type='text'
											className='form-control'
											id='local'
											onChange={this.handleChange}
											value={local}
											//required
										/>
									</div>
								</div>
								<div className='ml-2 col'>
									<div className='title mb-2'>
										<label>Data/hora:</label>
									</div>
									<div>
										<input
											type='datetime-local'
											className='form-control'
											id='dataHora'
											onChange={this.handleChange}
											value={dataHora}
											// required
										/>
									</div>
								</div>
							</div>
							<div className='form-row m-2'>
								<div className='ml-2 col'>
									<input
										type='submit'
										className='btn btn-outline-success'
										value='Gerar'
										onClick={this.toggle.bind(this)}
									/>
								</div>
							</div>
							<div className='QRModal'>
								<Modal
									isOpen={this.state.modal}
									toggle={this.toggle.bind(this)}
									modalTransition={{ timeout: 200 }}
									className='QRModalContent'>
									<ModalBody style={{ display: 'flex', justifyContent: 'center' }}>
										<QRCode style={{ width: 400 }} array={JSON.stringify(this.state)} />
									</ModalBody>
								</Modal>
							</div>
						</form>
					</Card>
				</div>

				<div className='container mt-3'>
					<Card fluid className='card-body mt-2 container p-2'>
						<ParticipantesList />
					</Card>
				</div>
			</div>
		)
	}
}

const mapStateToProps = state => ({
	itens: state.itensPalestras.newState,
})

const mapDispatchToProps = {
	dispatchItensPalestras: itensPalestras,
}

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(index)
