import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import './styles.css'

class index extends Component {
	constructor(props) {
		super(props)
		this.state = {}
	}
	render() {
		return (
			<>
				<div className='container' align='center'>
					<div className='p-5 mb-3 textMenu'>Escolha a opção para gerar o Qr_Code</div>
					<div className='row justify-content-md-center'>
						<div className='p-4'>
							<div className='buttom-menu-palestra'>
								<Link to='/palestra'>
									<img src={require('../../assets/palestra.png')} alt='Palestra' />
								</Link>
							</div>
						</div>
						<div className='p-4'>
							<div className='buttom-menu-frequencia'>
								<Link to='/frequencia'>
									<img src={require('../../assets/frequencia.png')} alt='Frequência' />
								</Link>
							</div>
						</div>
					</div>
				</div>
			</>
		)
	}
}

export default index
