import React from 'react'
import Header from '../../components/header'
import { GoogleMap, withScriptjs, withGoogleMap, Marker } from 'react-google-maps'
import { REACT_APP_GOOGLE_KEY } from '../../utils/maps'

import * as parksData from '../../data/skateboard-paks.json'

function Map() {
	return (
		<GoogleMap defaultZoom={12} defaultCenter={{ lat: 45.421532, lng: -75.697189 }}>
			{parksData.features.map(park => (
				<Marker
					key={park.properties.PARK_ID}
					position={{
						lat: park.geometry.coordinates[1],
						lng: park.geometry.coordinates[0],
					}}
				/>
			))}
		</GoogleMap>
	)
}

const WrappedMap = withScriptjs(withGoogleMap(Map))

function index() {
	return (
		<>
			<Header name='Ocorrencias' bg='#279566' />
			<div className='container pt-3'>
				<WrappedMap
					googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&
					libraries=geometry,drawing,places&key=${REACT_APP_GOOGLE_KEY}`}
					loadingElement={<div style={{ height: `100%` }} />}
					containerElement={<div style={{ height: `550px` }} />}
					mapElement={<div style={{ height: `100%` }} />}
				/>
			</div>
		</>
	)
}

export default index
