import 'bootstrap/dist/css/bootstrap.min.css'
import React, { Component } from 'react'
import { connect } from 'react-redux'

import './styles.css'
import { Card, Container, Segment, Input } from 'semantic-ui-react'
import { Modal, ModalBody } from 'reactstrap'
import Header from '../../components/header'
import QRCode from '../../components/qrcode'

class index extends Component {
	constructor(props) {
		super(props)
		this.state = {
			modal: false,
			selected: null,
		}
		this.cardList = this.cardList.bind(this)
	}

	toggle(qrcode) {
		this.setState({
			selected: qrcode,
			modal: !this.state.modal,
		})
	}

	cardList() {
		return this.props.valores.map(item => (
			<Card fluid className='card-body mt-2' style={{ height: 160 }} key={item.id}>
				<div className='form-row m-3'>
					<div className='ml-2'>
						<div className='title'>Código Turma</div>
						<div className='description'>{item.codTurma}</div>
					</div>
					<div className='ml-4'>
						<div className='title'>Descrição</div>
						<div className='description'>{item.descricaoTurma}</div>
					</div>
				</div>
				<div className='form-row'>
					<div className='ml-4'>
						<div className='title'>Dias da Semana</div>
						<div className='description-item'>{item.diasSemanas}</div>
					</div>
					<div className=' ml-4'>
						<div className='title'>Horário</div>
						<div className='description-item'>{item.horario}</div>
					</div>
					<div className=' ml-4'>
						<div className='title'>Professor(a)</div>
						<div className='description-item'>{item.professor}</div>
					</div>
					<div className=' ml-4'>
						<div className='title'>Unidade</div>
						<div className='description-item'>{item.unidade}</div>
					</div>
					<div className='QRModal'>
						<Modal
							isOpen={this.state.modal}
							toggle={this.toggle.bind(this, null)}
							modalTransition={{ timeout: 200 }}
							className='QRModalContent'>
							<ModalBody style={{ display: 'flex', justifyContent: 'center' }}>
								<QRCode style={{ width: 400 }} array={JSON.stringify(this.state.selected && this.state.selected)} />
							</ModalBody>
						</Modal>
					</div>
				</div>
				<div className='col position-absolute mt-3 d-flex justify-content-end'>
					<button onClick={this.toggle.bind(this, item)}>
						<QRCode style={{ height: 130 }} array={JSON.stringify(item)} />
					</button>
				</div>
			</Card>
		))
	}

	render() {
		return (
			<div>
				<Header name='Frequências' bg='rgb(60, 110, 163)' />
				<Container>
					<div className='overflow-auto m-3'>
						<Segment>
							<Input icon='search' size='large' placeholder='procurar...' />
						</Segment>
						<div className='card justify-content-center mt-3'>
							<div className='row no-gutters'>
								<div className='col-md col-sm-12 p-3' id='colQRform'>
									<Card.Group>{this.cardList()}</Card.Group>
								</div>
							</div>
						</div>
					</div>
				</Container>
			</div>
		)
	}
}

const mapStateToProps = state => ({
	valores: state.dataListReducer.valores,
})

export default connect(mapStateToProps)(index)
