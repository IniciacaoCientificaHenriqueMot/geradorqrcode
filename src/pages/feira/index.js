import React, { Component } from 'react'
import Header from '../../components/header'

import { Card } from 'semantic-ui-react'
import ParticipantesList from '../../components/participantes-list-feira'

export default class index extends Component {
	render() {
		return (
			<div>
				<Header name='Estácio - Feira do Conhecimento' bg='#00b9d3' />
				<div className='container'></div>

				<div className='container mt-3'>
					<Card fluid className='card-body mt-2 container p-2'>
						<ParticipantesList />
					</Card>
				</div>
			</div>
		)
	}
}
