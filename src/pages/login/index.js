import React, { Component } from 'react'
import './style.css'
import firebase from '../../service/api'
import { Link } from 'react-router-dom'
import { FaClipboardList } from 'react-icons/fa'
import { toast, ToastContainer } from 'react-toastify'

export default class Login extends Component {
	constructor(props) {
		super(props)
		this.state = {
			matriculaInput: '',
			nomeInput: '',
			cursoInput: '',
		}
	}

	handleChange = e => {
		this.setState({ [e.target.id]: e.target.value })
	}

	cadastrar() {
		const db = firebase.firestore()
		db.collection('presencas').add({
			matricula: this.state.matriculaInput,
			nome: this.state.nomeInput,
			curso: this.state.cursoInput,
			data: new Date(),
		})
		toast.success('Cadastro efetuado com sucesso!', {
			position: toast.POSITION.BOTTOM_CENTER,
		})
	}

	handleSubmit = e => {
		e.preventDefault()
		firebase
			.firestore()
			.collection('presencas')
			.where('matricula', '==', this.state.matriculaInput)
			.get()
			.then(doc => {
				if (doc.size === 0) {
					this.cadastrar()
				} else {
					toast.error('Usuário já cadastrado!', {
						position: toast.POSITION.BOTTOM_CENTER,
					})
				}
				this.setState({
					matriculaInput: '',
					nomeInput: '',
					cursoInput: '',
				})
			})
	}

	render() {
		return (
			<div className='fundo'>
				<div className='main-icon mt-3 ml-4'>
					<Link to='/feira'>
						<FaClipboardList />
					</Link>
				</div>
				<div className='fundo-inner pt-5'>
					<div className='row'>
						<img src={require('../../assets/Logo2019FDC3.png')} alt='logo Feira do Conhecimento' className='logo col' />
					</div>
					<form className=' text-center pt-5' onSubmit={this.handleSubmit}>
						<input
							type='text'
							id='matriculaInput'
							value={this.state.matriculaInput}
							placeholder='Telefone'
							className='form-control mb-4'
							onChange={this.handleChange}
							required
						/>
						<input
							type='text'
							id='nomeInput'
							placeholder='Nome'
							value={this.state.nomeInput}
							className='form-control mb-4'
							onChange={this.handleChange}
							required
						/>

						<input
							type='text'
							id='cursoInput'
							value={this.state.cursoInput}
							placeholder='Curso/Empresa'
							className='form-control'
							onChange={this.handleChange}
							required
						/>

						<div className='mt-5'>
							<input type='submit' value='Enviar' className='btn btn-outline-success' />
							<ToastContainer autoClose={3000} />
						</div>
					</form>
				</div>
			</div>
		)
	}
}
