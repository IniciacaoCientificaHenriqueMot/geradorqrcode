import { SELECIONAR_ITEM_LISTA, ITEM_SELECIONADO } from './actions-type'

export const selecionarItemLista = item => {
	return {
		type: SELECIONAR_ITEM_LISTA,
		payload: item,
	}
}

export const itemSelecionado = item => {
	return {
		type: ITEM_SELECIONADO,
		item,
	}
}
