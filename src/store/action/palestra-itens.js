import { ITENS_PALESTRAS } from './actions-type'
export const itensPalestras = value => ({
	type: ITENS_PALESTRAS,
	value,
})
