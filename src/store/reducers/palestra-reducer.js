import { ITENS_PALESTRAS } from '../action/actions-type'

export default function itensPalestras(state = [], action) {
	switch (action.type) {
		case ITENS_PALESTRAS:
			const newState = []
			return {
				...state,
				newState,
			}
		default:
			return state
	}
}
