import { combineReducers } from 'redux'

import dataListReducer from './data-list-reducer'
import itensPalestras from './palestra-reducer'

export default combineReducers({
	dataListReducer,
	itensPalestras,
})
