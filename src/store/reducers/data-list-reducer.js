
const INITIAL_STATE = {
  valores: [
    {
      id: 1,
      codTurma: '123',
      descricaoTurma: 'Tecnologias para dispositívos móveis',
      diasSemanas: 'seg-qua',
      horario: '18:00 - 20:00',
      professor: 'Henrique Mota',
      unidade: 'Moreira Campos'
    },
    {
      id: 2,
      codTurma: '456',
      descricaoTurma: 'Fundamentos de SI',
      diasSemanas: 'ter-qui',
      horario: '18:00 - 20:00',
      professor: 'Wellington Aguiar',
      unidade: 'Moreira Campos'
    },
    {
      id: 3,
      codTurma: '789',
      descricaoTurma: 'Modelagem de software',
      diasSemanas: 'seg',
      horario: '20:00 - 22:00',
      professor: 'Janete Amaral',
      unidade: 'Moreira Campos'
    },
    {
      id: 4,
      codTurma: '852',
      descricaoTurma: 'Sistemas Operacionais',
      diasSemanas: 'seg-qua',
      horario: '20:00 - 22:00',
      professor: 'Gerhard Saboia',
      unidade: 'Moreira Campos'
    }
  ],
};

export default function dataList(state = INITIAL_STATE, action) {
  switch (action.type) {
    default: return state;
  }

}