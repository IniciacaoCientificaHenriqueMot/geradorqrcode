import 'bootstrap/dist/css/bootstrap.min.css'
import 'semantic-ui-css/semantic.min.css'

import React from 'react'
import './styles.css'
import '../node_modules/react-toastify/dist/ReactToastify.css'

import { Provider } from 'react-redux'

import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Menu from './pages/menu'
import Palestra from './pages/palestra'
import Frequencia from './pages/frequencia'
import Feira from './pages/feira'
import Ocorrencias from './pages/ocorrencias'

import store from './store'
import Login from './pages/login'

function App() {
	return (
		<Provider store={store}>
			<BrowserRouter>
				<Switch>
					<Route exact path='/' component={Login} />
					<Route exact path='/menu' component={Menu} />
					<Route path='/feira' component={Feira} />
					<Route path='/palestra' component={Palestra} />
					<Route path='/frequencia' component={Frequencia} />
					<Route path='/ocorrencias' component={Ocorrencias} />
				</Switch>
			</BrowserRouter>
		</Provider>
	)
}

export default App
